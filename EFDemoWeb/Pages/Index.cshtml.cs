﻿using EFDataAccessLib.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EFDemoWeb.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger, IPeopleService myDependency,
       IEnumerable<IPeopleService> myDependencies, PeopleContext peopleContext)
        {
            List<Type> test = peopleContext.Model.GetEntityTypes().Select(t => t.ClrType).ToList();
            _logger = logger;
            Trace.WriteLine(myDependency is AnotherPeopleService);

            var dependencyArray = myDependencies.ToArray();
            Trace.WriteLine(dependencyArray[0] is PeopleService);
            Trace.WriteLine(dependencyArray[1] is AnotherPeopleService);
        }

        public void OnGet()
        {

        }
    }
}
