﻿using Microsoft.Extensions.Logging;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace EFDemoWeb
{
    public class ServiceProviderWrapper : IServiceProvider
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<ServiceProvider> logger;

        // Interesting enough to log, so lets raise events so that others can see what is being resolved
        public event EventHandler<ResolvingServiceEvent> ResolvingService;
        public event EventHandler<ResolvedServiceEvent> ResolvedService;
        public event EventHandler<UnResolvedServiceEvent> UnResolvedService;

        public ServiceProviderWrapper(IServiceProvider serviceProvider, ILogger<ServiceProvider> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;

            ResolvingService += OnResolvingService;
            ResolvedService += OnResolvedService;
            UnResolvedService += OnUnResolvedService;
        }

        public object GetService(Type serviceType)
        {
            ResolvingService?.Invoke(this, new ResolvingServiceEvent(serviceType));

            var resolvedService = serviceProvider.GetService(serviceType);

            if (resolvedService != null)
            {
                ResolvedService?.Invoke(this, new ResolvedServiceEvent(serviceType, resolvedService.GetType()));
            }
            else
            {
                UnResolvedService?.Invoke(this, new UnResolvedServiceEvent(serviceType));
            }


            return resolvedService;
        }
        

        private void OnResolvingService(object sender, ResolvingServiceEvent e)
            => this.logger.LogWarning($"Resolving service {e.ServiceType}.");                                       // Probably should be Trace level


        private void OnResolvedService(object sender, ResolvedServiceEvent e)
            => this.logger.LogWarning($"Resolved service {e.ServiceType} with an instance of {e.ResolvedType}.");   // Probably should be Information level

        private void OnUnResolvedService(object sender, UnResolvedServiceEvent e)
            => this.logger.LogWarning($"No instance of {e.ServiceType} could be resolved.");                        // Warning is an appropriate level
    }

    public class ResolvingServiceEvent : EventArgs
    {
        public Type ServiceType { get; }

        public ResolvingServiceEvent(Type serviceType) => ServiceType = serviceType;
    }

    public class ResolvedServiceEvent : EventArgs
    {
        public Type ServiceType { get; }
        public Type ResolvedType { get; }

        public ResolvedServiceEvent(Type serviceType, Type resolvedType)
        {
            ServiceType = serviceType;
            ResolvedType = resolvedType;
        }
    }

    public class UnResolvedServiceEvent : EventArgs
    {
        public Type ServiceType { get; }

        public UnResolvedServiceEvent(Type serviceType) => ServiceType = serviceType;
    }
}
