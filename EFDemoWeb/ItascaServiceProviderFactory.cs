﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace EFDemoWeb
{
    public class ItascaServiceProviderFactory : IServiceProviderFactory<IServiceCollection>
    {
        private readonly ServiceProviderOptions options;

        /// <summary>
        /// Initializes a new instance of the <see cref="ItascaServiceProviderFactory"/> class
        /// with default options.
        /// </summary>
        public ItascaServiceProviderFactory() : this(new ServiceProviderOptions())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItascaServiceProviderFactory"/> class
        /// with the specified <paramref name="options"/>.
        /// </summary>
        /// <param name="options">The options to use for this instance.</param>
        public ItascaServiceProviderFactory(ServiceProviderOptions options)
        {
            this.options = options ?? throw new ArgumentNullException(nameof(options));
        }

        /// <inheritdoc />
        public IServiceCollection CreateBuilder(IServiceCollection services) => services;

        /// <inheritdoc />
        public IServiceProvider CreateServiceProvider(IServiceCollection containerBuilder)
        {
            var serviceProvider = containerBuilder.BuildServiceProvider(options);
            var logger = serviceProvider.GetService<ILogger<ServiceProvider>>();
            return new ServiceProviderWrapper(serviceProvider, logger);
        }
    }
}
