﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace EFDemoWeb
{
    public class ServiceProviderBuilder
    {
        public void ResolveAndLog()
        {
            Console.WriteLine("Service Type is getting resolved");
        }
        public void ResolveAndLog(Type serviceType)
        {
            Console.WriteLine($"Resolving service type {serviceType.FullName}");
        }
        
    }
}
